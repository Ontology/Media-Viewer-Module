﻿Imports System.Reflection
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports Security_Module
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices
Imports System.Threading


Public Class frmSingleViewEmbedded

    Private objUserControl_SingleViewer As UserControl_SingleViewer

    Private objLocalConfig As clsLocalConfig

    Private objOItem_MediaItemType As clsOntologyItem
    Private objOItem_RefOrMediaItem As clsOntologyItem
    Private objOItem_File As clsOntologyItem

    Private Delegate Sub ActivateRefItemDelegate()
    Private ActiveRefItemHandler As ActivateRefItemDelegate

    Private objArgumentParsing As clsArgumentParsing

    Private threadModExchangeServer As Thread

    Private dateCreated As DateTime

    Private Sub ToolStripButton_Close_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Close.Click
        Close()
    End Sub

    Public Sub New(Globals As Globals, OItem_MediaItemType As clsOntologyItem)

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        objOItem_MediaItemType = OItem_MediaItemType
        Dim objAuthenticator = New frmAuthenticate(objLocalConfig.Globals, True, False, frmAuthenticate.ERelateMode.NoRelate)
        objAuthenticator.ShowDialog(Me)
        If objAuthenticator.DialogResult = Windows.Forms.DialogResult.OK Then
            objLocalConfig.OItem_User = objAuthenticator.OItem_User
            Initialize()
        End If

    End Sub

    Public Sub New(LocalConfig As clsLocalConfig, OItem_MediaItemType As clsOntologyItem)

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        objLocalConfig = LocalConfig
        objOItem_MediaItemType = OItem_MediaItemType
        If objLocalConfig.OItem_User Is Nothing Then
            Dim objAuthenticator = New frmAuthenticate(objLocalConfig.Globals, True, False, frmAuthenticate.ERelateMode.NoRelate)
            objAuthenticator.ShowDialog(Me)
            If objAuthenticator.DialogResult = Windows.Forms.DialogResult.OK Then
                objLocalConfig.OItem_User = objAuthenticator.OItem_User
            End If



        End If

        If Not objLocalConfig.OItem_User Is Nothing Then
            Initialize()
        End If
    End Sub

    Private Sub Initialize()
        objUserControl_SingleViewer = New UserControl_SingleViewer(objLocalConfig, objOItem_MediaItemType)
        objUserControl_SingleViewer.Dock = DockStyle.Fill
        ToolStripContainer1.ContentPanel.Controls.Add(objUserControl_SingleViewer)
        ActiveRefItemHandler = New ActivateRefItemDelegate(AddressOf ActiveRefItem)

    End Sub

    Private Sub ModExchangeHandler()
        AddHandler ModuleDataExchanger._serverResponse, AddressOf ExchangeAsyncHandler
        ModuleDataExchanger.Server(objLocalConfig)

    End Sub

    Private Sub ExchangeAsyncHandler(result As String)
        If Not String.IsNullOrEmpty(result) Then
            objArgumentParsing = New clsArgumentParsing(objLocalConfig.Globals, result.Split(" ").ToList(), True)
            If objArgumentParsing.OList_Items.Count = 1 Then
                If objArgumentParsing.OList_Items.First().Type.ToLower() = objLocalConfig.Globals.Type_Object.ToLower() Then
                    objOItem_RefOrMediaItem = objArgumentParsing.OList_Items.First()
                    ActiveRefItem()

                End If
            End If
        End If
        ModExchangeHandler()
    End Sub

    Private Sub ActiveRefItem()
        If InvokeRequired Then
            Invoke(ActiveRefItemHandler)
        Else
            If objOItem_File Is Nothing Then
                InitializeViewer(objOItem_RefOrMediaItem)
            Else
                InitializeViewer(objOItem_RefOrMediaItem, objOItem_File, dateCreated)
            End If
        End If
    End Sub

    Public Sub InitializeViewer(OItem_Ref As clsOntologyItem)
        objOItem_RefOrMediaItem = OItem_Ref
        objOItem_File = Nothing

        Me.Text = objOItem_RefOrMediaItem.Type & ": " & objOItem_RefOrMediaItem.Name
        Select Case objOItem_MediaItemType.GUID
            Case objLocalConfig.OItem_Type_Images__Graphic_.GUID
                objUserControl_SingleViewer.initialize_Image(objOItem_RefOrMediaItem)
            Case objLocalConfig.OItem_Type_PDF_Documents.GUID
                objUserControl_SingleViewer.initialize_PDF(objOItem_RefOrMediaItem)
            Case objLocalConfig.OItem_Type_Media_Item.GUID
                objUserControl_SingleViewer.initialize_MediaItem(objOItem_RefOrMediaItem)
        End Select
    End Sub

    Public Sub InitializeViewer(OItem_MediaItem As clsOntologyItem, OItem_File As clsOntologyItem, DateCreated As DateTime)
        objOItem_RefOrMediaItem = OItem_MediaItem
        objOItem_File = OItem_File
        Me.dateCreated = DateCreated
        Me.Text = objOItem_RefOrMediaItem.Name
        Select Case objOItem_MediaItemType.GUID
            Case objLocalConfig.OItem_Type_Images__Graphic_.GUID
                objUserControl_SingleViewer.initialize_Image(objOItem_RefOrMediaItem, objOItem_File, DateCreated)
            Case objLocalConfig.OItem_Type_PDF_Documents.GUID
                objUserControl_SingleViewer.initialize_PDF(objOItem_RefOrMediaItem, objOItem_File)
            Case objLocalConfig.OItem_Type_Media_Item.GUID
                objUserControl_SingleViewer.initialize_MediaItem(objOItem_RefOrMediaItem, objOItem_File, DateCreated)
        End Select
    End Sub

    Private Sub frmSingleViewEmbedded_Load(sender As Object, e As EventArgs) Handles Me.Load
        If objLocalConfig.OItem_User Is Nothing Then
            Close()
        End If
    End Sub

    Private Sub ListenToolStripMenuItem_Listen_CheckStateChanged(sender As Object, e As EventArgs) Handles ListenToolStripMenuItem_Listen.CheckStateChanged
        If (ListenToolStripMenuItem_Listen.Checked) Then
            Try
                threadModExchangeServer.Abort()
                ModuleDataExchanger.Disconnect()
            Catch ex As Exception

            End Try
            threadModExchangeServer = New Thread(AddressOf ModExchangeHandler)
            threadModExchangeServer.Start()
        Else
            Try
                threadModExchangeServer.Abort()
                ModuleDataExchanger.Disconnect()
            Catch ex As Exception

            End Try
        End If
    End Sub
End Class