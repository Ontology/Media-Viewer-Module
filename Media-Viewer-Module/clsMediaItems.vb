﻿Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports Filesystem_Module
Imports System.IO
Imports System.Reflection
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices

Public Class clsMediaItems
    Private objDataWork_Images As clsDataWork_Images
    Private objDataWork_MediaItems As clsDataWork_MediaItem
    Private objDataWork_PDFs As clsDataWork_PDF

    Private objDBLevel_Ref As OntologyModDBConnector
    Private objDBLevel_File As OntologyModDBConnector
    Private objDBLevel_Created As OntologyModDBConnector

    Private objLocalConfig As clsLocalConfig

    Public Property BlobConnection As clsBlobConnection

    Public Sub New(LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig

        initialize()
    End Sub

    Public Sub New(LocalConfig As clsLocalConfig, blobConnection As clsBlobConnection)
        objLocalConfig = LocalConfig
        Me.BlobConnection = blobConnection

        initialize()
    End Sub

    Public Sub New(Globals As Globals)
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        initialize()
    End Sub

    Public Sub New(Globals As Globals, blobConnection As clsBlobConnection)
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        Me.BlobConnection = blobConnection

        initialize()
    End Sub

    Public Function has_Images(OItem_Ref As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result = objDataWork_Images.hasImage(OItem_Ref)

        Return objOItem_Result
    End Function

    Public Function has_MediaItems(OItem_Ref As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result = objDataWork_MediaItems.hasMediaItem(OItem_Ref)

        Return objOItem_Result
    End Function

    Public Function has_PDFs(OItem_Ref As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result = objDataWork_PDFs.hasPdf(OItem_Ref)

        Return objOItem_Result
    End Function

    Public Function Get_ImageFile(OItem_File As clsOntologyItem) As Image
        Dim objImage As Image = Nothing

        OItem_File.Additional1 = "%Temp%\" & objLocalConfig.Globals.NewGUID & Path.GetExtension(OItem_File.Name)

        OItem_File.Additional1 = Environment.ExpandEnvironmentVariables(OItem_File.Additional1)

        Dim objOItem_Result = BlobConnection.save_Blob_To_File(OItem_File, OItem_File.Additional1)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            Try
                objImage = New Bitmap(OItem_File.Additional1)

            Catch ex As Exception

            End Try
        End If

        Return objImage
    End Function

    Public Function Get_MultiMediaItem(OItem_MediaItem As clsOntologyItem) As clsMultiMediaItem

        Dim objRandom As New Random


        Dim objOL_MediaItems_To_File = New List(Of clsObjectRel) From {New clsObjectRel With { _
            .ID_Object = OItem_MediaItem.GUID, _
            .ID_Parent_Other = objLocalConfig.OItem_Type_File.GUID, _
            .ID_RelationType = objLocalConfig.OItem_RelationType_belonging_Source.GUID}}


        Dim objOItem_Result = objDBLevel_File.GetDataObjectRel(objOL_MediaItems_To_File, _
                                            doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            If objDBLevel_File.ObjectRels.Any Then
                Dim objOL_CreateDate = objDBLevel_File.ObjectRels.Select(Function(fi) New clsObjectAtt With {.ID_Object = fi.ID_Other, _
                                                                                                        .ID_AttributeType = objLocalConfig.OItem_Attribute_Datetimestamp__Create_.GUID}).ToList()


                objOItem_Result = objDBLevel_Created.GetDataObjectAtt(objOL_CreateDate, doIds:=False)

                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    Dim objLMediaItems = (From objFile In objDBLevel_File.ObjectRels
                                Group Join objAttrib In objDBLevel_Created.ObjAtts On objAttrib.ID_Object Equals objFile.ID_Other Into objAttribs = Group
                                From objAttrib In objAttribs.DefaultIfEmpty
                                Select New clsMultiMediaItem(OItem_MediaItem.GUID, _
                                                            OItem_MediaItem.Name, _
                                                            OItem_MediaItem.GUID_Parent, _
                                                            objFile.ID_Other, _
                                                            objFile.Name_Other, _
                                                            objFile.ID_Parent_Other, _
                                                            If(Not objAttrib Is Nothing, objAttrib, Nothing), _
                                                            0, _
                                                            0)).ToList()

                    If objLMediaItems.Any Then
                        Return objLMediaItems.First()
                    Else
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        Else
            Return Nothing
        End If

    End Function



    Private Sub initialize()
        objDataWork_Images = New clsDataWork_Images(objLocalConfig)
        objDataWork_MediaItems = New clsDataWork_MediaItem(objLocalConfig)
        objDataWork_PDFs = New clsDataWork_PDF(objLocalConfig)

        objDBLevel_Ref = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Created = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_File = New OntologyModDBConnector(objLocalConfig.Globals)

        If BlobConnection Is Nothing Then
            BlobConnection = New clsBlobConnection(objLocalConfig.Globals)
        End If

    End Sub
End Class
